const pkg = require('./package')
const axios = require('axios')

module.exports = {
  mode: 'universal',

  /*
  ** Headers of the page
  */
  head: {
    title: "My first storybook + Nuxt app",
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Lato:400,700'}
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#3B8070' },

  /*
  ** Global CSS
  */
  css: [
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    ['storyblok-nuxt', {accessToken: process.env.NODE_ENV == 'production' ? 'iqwaWwCy67QYjZWuzpXqggtt':'gvcXd4XfhrOOu7GfvV1EdQtt', cacheProvider: 'memory'}]
  ],

  generate: {
    routes: function(){
      axios.get('https://api.storyblok.com/v1/cdn/stories?version=published&token=iqwaWwCy67QYjZWuzpXqggtt&starts_with=blog&cv=' + Math.floor(Date.now() / 1e3))
        .then(res => {
          const posts = res.data.stories.map(bp => bp.full_slug);
          return ['/', '/blog', '/about', ...posts];
        })
    }
  },

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {

    }
  }
}
